<?php


namespace Training\Module2\Model;


interface ModifierInterface
{
    /**
     * @param $value - incoming value
     * @return mixed - modified result
     */
    public function modify($value);
}
