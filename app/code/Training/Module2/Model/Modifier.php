<?php


namespace Training\Module2\Model;

/**
 * Class adding prefix and postfix to the incoming value
 * @package Training\Module2\Model
 */
class Modifier implements ModifierInterface
{
    public $prefix;
    public $postfix;

    public function __construct($prefix, $postfix)
    {
        $this->prefix = $prefix;
        $this->postfix = $postfix;
    }

    public function modify($value)
    {
        return $this->prefix . $value . $this->postfix;
    }
}
