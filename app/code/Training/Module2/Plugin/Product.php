<?php


namespace Training\Module2\Plugin;

/**
 * Class adding different prefixes and postfixes to the incoming sku and name of a product
 * @package Training\Module2\Plugin
 */
class Product
{
    private $_nameModifier;
    private $_skuModifier;

    public function __construct(
        \Training\Module2\Model\ModifierInterface $nameModifier,
        \Training\Module2\Model\ModifierInterface $skuModifier
    ) {
        $this->_nameModifier = $nameModifier;
        $this->_skuModifier = $skuModifier;
    }

    public function afterGetName(\Magento\Catalog\Model\Product $subject, $result) {
        return $this->_nameModifier->modify($result);
    }

    public function afterGetSku(\Magento\Catalog\Model\Product $subject, $result) {
        return $this->_skuModifier->modify($result);
    }
}
