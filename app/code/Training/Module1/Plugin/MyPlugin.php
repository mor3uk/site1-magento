<?php

namespace Training\Module1\Plugin;

/**
 * A custom training plugin
 * @package Training\Module1\Plugin
 */
class MyPlugin
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Catalog\Model\Product $subject - observed object
     * @param $result - observed function result
     * @return mixed - modified result
     */
    public function afterGetPrice(\Magento\Catalog\Model\Product $subject, $result)
    {
        $additionEnabled = $this->_scopeConfig->getValue('training_section/price_addition/enable');
        if ($additionEnabled) {
            $additionAmount =  (float) $this->_scopeConfig->getValue('training_section/price_addition/addition_amount');
            return $result + $additionAmount;
        }

        return $result;
    }
}
