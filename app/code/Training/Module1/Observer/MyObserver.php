<?php

namespace Training\Module1\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * A custom training observer
 * @package Training\Module1\Observer
 */
class MyObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_request = $request;
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $currentUrl = $this->_request->getPathInfo();
        $this->logger->info($currentUrl);
    }
}
